package org.miralles.ryanair.infrastructure.repository.rest;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.miralles.ryanair.domain.entity.Route;
import org.miralles.ryanair.infrastructure.adapter.RouteResponse2DomainAdapter;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

@RunWith(JUnitPlatform.class)
class RoutesRyanairRepositoryTest {
    private static final int ZERO = 0;
    private static final String DEPARTURE_IATA = "FCO";
    private static final String ARRIVAL_IATA = "BCN";
    private RoutesFilter routesFilter = new RoutesFilter();//TODO mock!
    private RoutesRestClient routesRestClient = new RoutesRestClient(); //TODO mock!
    private RouteResponse2DomainAdapter routeResponse2DomainAdapter = new RouteResponse2DomainAdapter(); //TODO mock!

    @Test
    @Ignore
    void callToRoutesRestService() {
        RoutesRyanairRepository routesRyanairRepository =
                new RoutesRyanairRepository(routesFilter, routesRestClient, routeResponse2DomainAdapter);

        List<Route> routes =
                routesRyanairRepository.getRouteByDepartureIataAndArrivalIata(DEPARTURE_IATA, ARRIVAL_IATA);

        assertThat(routes.size(), greaterThan(ZERO));
    }

}