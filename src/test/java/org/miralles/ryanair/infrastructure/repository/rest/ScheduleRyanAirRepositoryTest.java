package org.miralles.ryanair.infrastructure.repository.rest;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.miralles.ryanair.domain.entity.TimeTable;
import org.miralles.ryanair.domain.port.secondary.ScheduleRepository;
import org.miralles.ryanair.infrastructure.adapter.ScheduleDayResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.adapter.ScheduleFlightResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.adapter.ScheduleResponse2DomainAdapter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(JUnitPlatform.class)
class ScheduleRyanAirRepositoryTest {

    private static final String DEPARTURE_IATA = "DUB";
    private static final String ARRIVAL_IATA = "WRO";
    private static final int YEAR = 2018;
    private static final int MONTH = 6;
    private static final int ANY_MONTH = 6;
    private ScheduleRestClient scheduleRestClient = new ScheduleRestClient(); //TODO mock
    private ScheduleFlightResponse2DomainAdapter scheduleFlightResponse2DomainAdapter = new ScheduleFlightResponse2DomainAdapter();
    private ScheduleDayResponse2DomainAdapter scheduleDayResponse2DomainAdapter = new ScheduleDayResponse2DomainAdapter(scheduleFlightResponse2DomainAdapter);//TODO mock
    private ScheduleResponse2DomainAdapter scheduleResponse2DomainAdapter = new ScheduleResponse2DomainAdapter(scheduleDayResponse2DomainAdapter); //TODO mock

    @Ignore
    void callToScheduleRestService() {
        ScheduleRepository scheduleRepository =
                new ScheduleRyanAirRepository(scheduleRestClient, scheduleResponse2DomainAdapter);

        TimeTable response =
                scheduleRepository.getTimeTableByYearMonthAndIatas(DEPARTURE_IATA, ARRIVAL_IATA, YEAR, MONTH);

        assertThat(response, notNullValue());
        assertThat(response.month, equalTo(ANY_MONTH));
    }
}