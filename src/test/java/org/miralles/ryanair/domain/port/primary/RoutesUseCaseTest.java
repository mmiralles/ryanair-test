package org.miralles.ryanair.domain.port.primary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.miralles.ryanair.domain.entity.Route;
import org.miralles.ryanair.domain.port.secondary.RoutesRepository;
import org.miralles.ryanair.infrastructure.MockitoExtension;
import org.mockito.Mock;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class RoutesUseCaseTest {
    private static final String DEPARTURE_IATA = "ANY_DEPARTURE_IATA";
    private static final String ARRIVAL_IATA = "ANY_ARRIVAL_IATA";
    private static final String ANY_IATA = "ANY_IATA";
    @Mock
    private RoutesRepository routesRepository;

    @Test
    void isANotInterconnectedRoute() {
        when(routesRepository.getRouteByDepartureIataAndArrivalIata(DEPARTURE_IATA, ARRIVAL_IATA))
                .thenReturn(singletonList(Route.builder().connectingAirport(ANY_IATA).build())); //TODO create a fixture
        RoutesUseCase routesUseCase = new RoutesUseCase(routesRepository);

        Boolean isInterConnected = routesUseCase.isRouteNotInterConnected(DEPARTURE_IATA, ARRIVAL_IATA);

        assertThat(isInterConnected, equalTo(false));
    }

    @Test
    void isAInterconnectedRoute() {
        when(routesRepository.getRouteByDepartureIataAndArrivalIata(DEPARTURE_IATA, ARRIVAL_IATA))
                .thenReturn(singletonList(Route.builder().connectingAirport(null).build())); //TODO create a fixture
        RoutesUseCase routesUseCase = new RoutesUseCase(routesRepository);

        Boolean isInterConnected = routesUseCase.isRouteNotInterConnected(DEPARTURE_IATA, ARRIVAL_IATA);

        assertThat(isInterConnected, equalTo(true));
    }

}