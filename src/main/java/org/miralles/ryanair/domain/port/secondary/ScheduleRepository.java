package org.miralles.ryanair.domain.port.secondary;

import org.miralles.ryanair.domain.entity.TimeTable;

public interface ScheduleRepository {
    TimeTable getTimeTableByYearMonthAndIatas(final String departureIata,
                                              final String arrivalIata,
                                              final Integer year,
                                              final Integer month);
}
