package org.miralles.ryanair.domain.port.secondary;

import org.miralles.ryanair.domain.entity.Route;

import java.util.List;

public interface RoutesRepository {
    List<Route> getRouteByDepartureIataAndArrivalIata(final String departureIata, final  String arrivaIata);
}
