package org.miralles.ryanair.domain.port.primary;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.miralles.ryanair.domain.entity.FlightAvailability;

import java.util.List;

@Data
@AllArgsConstructor
public class FlightAvailabilityResponse {
    private final List<FlightAvailability> response;
}
