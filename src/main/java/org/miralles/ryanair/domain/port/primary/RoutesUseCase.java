package org.miralles.ryanair.domain.port.primary;

import org.miralles.ryanair.domain.entity.Route;
import org.miralles.ryanair.domain.port.secondary.RoutesRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class RoutesUseCase {

    private RoutesRepository routesRepository;

    public RoutesUseCase(RoutesRepository routesRepository) {
        this.routesRepository = routesRepository;
    }

    public Boolean isRouteNotInterConnected(String departureIata, String arrivalIata) {

        List<Route> routes = routesRepository.getRouteByDepartureIataAndArrivalIata(departureIata, arrivalIata);

        List<Route> routesFiltered = getRoutesFilteredByConnectigAirport(routes);

        return routesFiltered.size()>0;
    }

    private List<Route> getRoutesFilteredByConnectigAirport(List<Route> routes) {
        return routes.stream()
                    .filter(route -> route.connectingAirport == null)
                    .collect(toList());
    }
}
