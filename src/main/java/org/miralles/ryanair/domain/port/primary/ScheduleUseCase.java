package org.miralles.ryanair.domain.port.primary;

import org.miralles.ryanair.domain.entity.TimeTable;
import org.miralles.ryanair.domain.port.secondary.ScheduleRepository;

public class ScheduleUseCase {
    private ScheduleRepository scheduleRepository;

    public ScheduleUseCase(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    public TimeTable getTimeTableByIatasYearAndMonth(final String arrivalIata,
                                                     final String departureIata,
                                                     final Integer year,
                                                     final Integer month) {
        return scheduleRepository.getTimeTableByYearMonthAndIatas(departureIata, arrivalIata, year, month);
    }
}
