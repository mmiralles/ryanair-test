package org.miralles.ryanair.domain.entity;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Leg {
    private String departureAirport;
    private String arrivalAirport;
    private String departureDateTime;
    private String arrivalDateTime;
}
