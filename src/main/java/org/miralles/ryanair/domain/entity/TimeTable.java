package org.miralles.ryanair.domain.entity;

import lombok.Builder;
import lombok.ToString;

import java.util.List;

@Builder
@ToString
public class TimeTable {
    public Integer month;
    public List<Day> days;
}