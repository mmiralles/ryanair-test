package org.miralles.ryanair.domain.entity;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Flight {
    public String number;
    public String departureTime;
    public String arrivalTime;
}