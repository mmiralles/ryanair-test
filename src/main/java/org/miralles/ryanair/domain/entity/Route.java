package org.miralles.ryanair.domain.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Route {
    public String airportFrom;
    public String airportTo;
    public String connectingAirport;
    public Boolean newRoute;
    public Boolean seasonalRoute;
    public String group;
}
