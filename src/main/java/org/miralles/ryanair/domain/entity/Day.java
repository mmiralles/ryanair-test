package org.miralles.ryanair.domain.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder
@ToString
@Getter
public class Day {
    public Integer day;
    public List<Flight> flights;
}
