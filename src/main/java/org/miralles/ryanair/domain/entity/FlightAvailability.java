package org.miralles.ryanair.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@AllArgsConstructor
@Data
public class FlightAvailability {
    private Integer stops;
    private List<Leg> legs;
}
