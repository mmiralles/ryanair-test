package org.miralles.ryanair.infrastructure.repository.rest;

import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class RoutesRestClient {
    public List<RyanAirRouteResponseEntity> getRyanAirRoutes() {
        String uri = "https://api.ryanair.com/core/3/routes"; //TODO extract into yaml file
        RestTemplate restTemplate = new RestTemplate();//TODO inject
        RyanAirRouteResponseEntity[] result = restTemplate.getForObject(uri, RyanAirRouteResponseEntity[].class);
        return Arrays.asList(result);
    }
}
