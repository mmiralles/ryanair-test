package org.miralles.ryanair.infrastructure.repository.rest;

import lombok.Getter;

import java.util.List;

@Getter
public class RyanAirScheduleResponseEntity {
    public Integer month;
    public List<RyanAirScheduleDayResponseEntity> days;
}
