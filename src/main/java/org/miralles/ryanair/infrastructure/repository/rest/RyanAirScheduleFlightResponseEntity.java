package org.miralles.ryanair.infrastructure.repository.rest;

import lombok.Getter;

@Getter
public class RyanAirScheduleFlightResponseEntity {
    public String number;
    public String departureTime;
    public String arrivalTime;
}