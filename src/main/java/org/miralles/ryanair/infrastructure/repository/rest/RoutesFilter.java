package org.miralles.ryanair.infrastructure.repository.rest;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class RoutesFilter {
    public List<RyanAirRouteResponseEntity> filterRoutesByDepartureAndArrival(final String departureIata,
                                                                              final String arrivalIata,
                                                                              final List<RyanAirRouteResponseEntity> routes) {
        return routes.stream()
                .filter(route -> route.getAirportFrom().equals(departureIata))
                .filter(route -> route.getAirportTo().equals(arrivalIata))
                .collect(toList());
    }
}
