package org.miralles.ryanair.infrastructure.repository.rest;

import org.springframework.web.client.RestTemplate;

public class ScheduleRestClient {

    private static final String SEPARATOR = "/";
    private static final String MONTHS = "months";
    private static final String YEARS = "years";
    private static final String DOMAIN_URL = "https://api.ryanair.com/timetable/3/schedules/"; //TODO put into configuration file

    public RyanAirScheduleResponseEntity getRyanAirRoutes(final String departureIata,
                                                          final String arrivalIata,
                                                          final Integer year,
                                                          final Integer month) {

        RestTemplate restTemplate = new RestTemplate();//TODO put into the context

        String uri = composeUri(departureIata, arrivalIata, year, month);

        return restTemplate.getForObject(uri, RyanAirScheduleResponseEntity.class);
    }

    private String composeUri(final String departureIata,
                              final String arrivalIata,
                              final Integer year,
                              final Integer month) {
        return new StringBuilder()
                    .append(DOMAIN_URL)
                    .append(departureIata)
                    .append(SEPARATOR)
                    .append(arrivalIata)
                    .append(SEPARATOR)
                    .append(YEARS)
                    .append(SEPARATOR)
                    .append(year)
                    .append(SEPARATOR)
                    .append(MONTHS)
                    .append(SEPARATOR)
                    .append(month)
                    .toString();
    }
}
