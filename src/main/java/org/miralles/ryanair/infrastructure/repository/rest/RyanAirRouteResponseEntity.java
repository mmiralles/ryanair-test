package org.miralles.ryanair.infrastructure.repository.rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RyanAirRouteResponseEntity {
    public String airportFrom;
    public String airportTo;
    public String connectingAirport;
    public Boolean newRoute;
    public Boolean seasonalRoute;
    public String group;
}
