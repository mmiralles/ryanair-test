package org.miralles.ryanair.infrastructure.repository.rest;

import org.miralles.ryanair.domain.entity.Route;
import org.miralles.ryanair.domain.port.secondary.RoutesRepository;
import org.miralles.ryanair.infrastructure.adapter.RouteResponse2DomainAdapter;

import java.util.List;

public class RoutesRyanairRepository implements RoutesRepository {

    private RoutesFilter routesFilter;
    private RoutesRestClient routesRestClient;
    private RouteResponse2DomainAdapter routeResponse2DomainAdapter;

    public RoutesRyanairRepository(final RoutesFilter routesFilter,
                                   final RoutesRestClient restClient,
                                   final RouteResponse2DomainAdapter routeResponse2DomainAdapter) {
        this.routesFilter = routesFilter;
        this.routesRestClient = restClient;
        this.routeResponse2DomainAdapter = routeResponse2DomainAdapter;
    }

    @Override
    public List<Route> getRouteByDepartureIataAndArrivalIata(final String departureIata,
                                                             final String arrivalIata) {
        List<RyanAirRouteResponseEntity> routes = getRyanAirRoutes();

        List<RyanAirRouteResponseEntity> routesFiltered =
                routesFilter.filterRoutesByDepartureAndArrival(departureIata, arrivalIata, routes);

        return routeResponse2DomainAdapter.execute(routesFiltered);
    }

    private List<RyanAirRouteResponseEntity> getRyanAirRoutes() {
        return routesRestClient.getRyanAirRoutes();
    }
}
