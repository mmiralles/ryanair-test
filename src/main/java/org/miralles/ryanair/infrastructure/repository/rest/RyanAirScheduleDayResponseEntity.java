package org.miralles.ryanair.infrastructure.repository.rest;

import lombok.Getter;

import java.util.List;

@Getter
public class RyanAirScheduleDayResponseEntity {
    public Integer day;
    public List<RyanAirScheduleFlightResponseEntity> flights;
}
