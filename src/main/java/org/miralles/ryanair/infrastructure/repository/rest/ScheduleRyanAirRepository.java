package org.miralles.ryanair.infrastructure.repository.rest;

import org.miralles.ryanair.domain.entity.TimeTable;
import org.miralles.ryanair.domain.port.secondary.ScheduleRepository;
import org.miralles.ryanair.infrastructure.adapter.ScheduleResponse2DomainAdapter;

public class ScheduleRyanAirRepository implements ScheduleRepository {

    private ScheduleRestClient scheduleRestClient;
    private ScheduleResponse2DomainAdapter scheduleResponse2DomainAdapter;

    public ScheduleRyanAirRepository(ScheduleRestClient scheduleRestClient, ScheduleResponse2DomainAdapter scheduleResponse2DomainAdapter) {
        this.scheduleRestClient = scheduleRestClient;
        this.scheduleResponse2DomainAdapter = scheduleResponse2DomainAdapter;
    }

    @Override
    public TimeTable getTimeTableByYearMonthAndIatas(final String departureIata,
                                                     final String arrivalIata,
                                                     final Integer year,
                                                     final Integer month) {
        RyanAirScheduleResponseEntity timeTable =
                scheduleRestClient.getRyanAirRoutes(departureIata, arrivalIata, year, month);

        return scheduleResponse2DomainAdapter.execute(timeTable);
    }
}
