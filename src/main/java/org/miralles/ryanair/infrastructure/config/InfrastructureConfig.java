package org.miralles.ryanair.infrastructure.config;

import org.miralles.ryanair.domain.port.secondary.RoutesRepository;
import org.miralles.ryanair.domain.port.secondary.ScheduleRepository;
import org.miralles.ryanair.infrastructure.adapter.AvailabilityAdapter;
import org.miralles.ryanair.infrastructure.adapter.RouteResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.adapter.ScheduleDayResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.adapter.ScheduleFlightResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.adapter.ScheduleResponse2DomainAdapter;
import org.miralles.ryanair.infrastructure.repository.rest.RoutesFilter;
import org.miralles.ryanair.infrastructure.repository.rest.RoutesRestClient;
import org.miralles.ryanair.infrastructure.repository.rest.RoutesRyanairRepository;
import org.miralles.ryanair.infrastructure.repository.rest.ScheduleRestClient;
import org.miralles.ryanair.infrastructure.repository.rest.ScheduleRyanAirRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfrastructureConfig {

    @Bean
    public RoutesFilter routesFilter() {
        return new RoutesFilter();
    }

    @Bean
    public RoutesRestClient routesRestClient() {
        return new RoutesRestClient();
    }

    @Bean
    public RouteResponse2DomainAdapter routeResponse2DomainAdapter() {
        return new RouteResponse2DomainAdapter();
    }

    @Bean
    public ScheduleRestClient scheduleRestClient() {
        return new ScheduleRestClient();
    }

    @Bean
    public ScheduleFlightResponse2DomainAdapter scheduleFlightResponse2DomainAdapter(){
        return new ScheduleFlightResponse2DomainAdapter();
    }

    @Bean
    public ScheduleDayResponse2DomainAdapter scheduleDayResponse2DomainAdapter(final ScheduleFlightResponse2DomainAdapter scheduleFlightResponse2DomainAdapter){
        return new ScheduleDayResponse2DomainAdapter(scheduleFlightResponse2DomainAdapter);
    }

    @Bean
    public ScheduleResponse2DomainAdapter scheduleResponse2DomainAdapter(final ScheduleDayResponse2DomainAdapter scheduleDayResponse2DomainAdapter) {
        return new ScheduleResponse2DomainAdapter(scheduleDayResponse2DomainAdapter);
    }

    @Bean
    public AvailabilityAdapter availabilityAdapter(){
        return new AvailabilityAdapter();
    }

    @Bean
    public ScheduleRepository schedulesRepository(final ScheduleRestClient scheduleRestClient,
                                                  final ScheduleResponse2DomainAdapter scheduleResponse2DomainAdapter) {
        return new ScheduleRyanAirRepository(scheduleRestClient, scheduleResponse2DomainAdapter);
    }

    @Bean
    public RoutesRepository routesRepository(final RoutesFilter routesFilter,
                                             final RoutesRestClient routesRestClient,
                                             final RouteResponse2DomainAdapter routeResponse2DomainAdapter) {
        return new RoutesRyanairRepository(routesFilter, routesRestClient, routeResponse2DomainAdapter);
    }
}
