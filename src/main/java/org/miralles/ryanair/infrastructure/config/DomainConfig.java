package org.miralles.ryanair.infrastructure.config;

import org.miralles.ryanair.domain.port.primary.RoutesUseCase;
import org.miralles.ryanair.domain.port.primary.ScheduleUseCase;
import org.miralles.ryanair.domain.port.secondary.RoutesRepository;
import org.miralles.ryanair.domain.port.secondary.ScheduleRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {
    @Bean
    public RoutesUseCase routesUseCase(final RoutesRepository routesRepository){
        return new RoutesUseCase(routesRepository);
    }

    @Bean
    public ScheduleUseCase scheduleUseCase(final ScheduleRepository scheduleRepository){
        return new ScheduleUseCase(scheduleRepository);
    }
}
