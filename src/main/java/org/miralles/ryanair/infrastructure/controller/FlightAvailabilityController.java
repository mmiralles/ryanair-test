package org.miralles.ryanair.infrastructure.controller;

import org.miralles.ryanair.domain.entity.Day;
import org.miralles.ryanair.domain.entity.FlightAvailability;
import org.miralles.ryanair.domain.entity.TimeTable;
import org.miralles.ryanair.domain.port.primary.FlightAvailabilityResponse;
import org.miralles.ryanair.domain.port.primary.RoutesUseCase;
import org.miralles.ryanair.domain.port.primary.ScheduleUseCase;
import org.miralles.ryanair.infrastructure.adapter.AvailabilityAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Collectors.toList;

@RestController
public class FlightAvailabilityController {

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
    @Autowired
    private RoutesUseCase routesUseCase;
    @Autowired
    private ScheduleUseCase scheduleUseCase;
    @Autowired
    private AvailabilityAdapter availabilityAdapter;

    @RequestMapping(value = "/ryanair/interconnections", produces = "application/json")
    public FlightAvailabilityResponse interconnections(@RequestParam(value = "departure", defaultValue = "MAD") String departureIata,
                                                       @RequestParam(value = "arrival", defaultValue = "BCN") String arrivalIata,
                                                       @RequestParam(value = "departureDateTime", defaultValue = "2018-03-01T07:00") String departureDateTime,
                                                       @RequestParam(value = "arrivalDateTime", defaultValue = "2018-03-01T07:00") String arrivalDateTime) {

        FlightAvailabilityResponse response;

        Boolean routes = routesUseCase.isRouteNotInterConnected(departureIata, arrivalIata);
        if (routes) {
            //TODO refactor those lines working with java 8 dateTime in a better way
            LocalDateTime departureLocalDateTime = convertToLocalDateTime(departureDateTime);
            LocalDateTime arrivalLocalDateTime = convertToLocalDateTime(arrivalDateTime);
            Integer month = obtainMonth(departureLocalDateTime);
            Integer year = obtainYear(arrivalLocalDateTime);
            Integer departureDay = departureLocalDateTime.getDayOfMonth();
            Integer arrivalDay = arrivalLocalDateTime.getDayOfMonth();

            TimeTable allPossibleFlights = scheduleUseCase.getTimeTableByIatasYearAndMonth(arrivalIata, departureIata, year, month);
            List<Day> daysInRange = getDaysRange(departureDay, arrivalDay, allPossibleFlights);

            //TODO filter also by flight time.
            //TODO dataclump
            List<FlightAvailability> flightsAvailability = availabilityAdapter.execute(departureIata, arrivalIata, month, year, daysInRange);

            response = new FlightAvailabilityResponse(flightsAvailability);
        } else {
            response = new FlightAvailabilityResponse(EMPTY_LIST);
        }
        return response;
    }

    private LocalDateTime convertToLocalDateTime(final String dateTime) {
        return LocalDateTime.parse(dateTime, formatter);
    }

    private List<Day> getDaysRange(Integer departureDay, Integer arrivalDay, TimeTable allPossibleFlights) {
        return allPossibleFlights.days.stream()
                .filter(day -> day.getDay() >= departureDay)
                .filter(day -> day.getDay() <= arrivalDay)
                .collect(toList());
    }

    private int obtainYear(final LocalDateTime departureDateTime) {
        return departureDateTime.getYear();
    }

    private int obtainMonth(final LocalDateTime departureDateTime) {
        return departureDateTime.getMonth().getValue();
    }
}
