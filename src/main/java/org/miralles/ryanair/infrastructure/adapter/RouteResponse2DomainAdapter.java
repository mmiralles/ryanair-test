package org.miralles.ryanair.infrastructure.adapter;

import org.miralles.ryanair.domain.entity.Route;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirRouteResponseEntity;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class RouteResponse2DomainAdapter {
    public List<Route> execute(List<RyanAirRouteResponseEntity> routesFiltered) {
        return routesFiltered.stream()
                .map(responseRoute -> Route.builder()
                        .connectingAirport(responseRoute.getConnectingAirport())
                        .group(responseRoute.getGroup())
                        .seasonalRoute(responseRoute.getSeasonalRoute())
                        .newRoute(responseRoute.getNewRoute())
                        .airportTo(responseRoute.getAirportTo())
                        .airportFrom(responseRoute.getAirportFrom())
                        .build())
                .collect(toList());
    }
}
