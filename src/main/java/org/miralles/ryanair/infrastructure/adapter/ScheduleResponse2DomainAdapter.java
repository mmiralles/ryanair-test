package org.miralles.ryanair.infrastructure.adapter;

import org.miralles.ryanair.domain.entity.Day;
import org.miralles.ryanair.domain.entity.TimeTable;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirScheduleDayResponseEntity;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirScheduleResponseEntity;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ScheduleResponse2DomainAdapter {

    private ScheduleDayResponse2DomainAdapter scheduleDayResponse2DomainAdapter;

    public ScheduleResponse2DomainAdapter(ScheduleDayResponse2DomainAdapter scheduleDayResponse2DomainAdapter) {
        this.scheduleDayResponse2DomainAdapter = scheduleDayResponse2DomainAdapter;
    }

    public TimeTable execute(final RyanAirScheduleResponseEntity responseEntity) {

        return TimeTable.builder()
                .month(responseEntity.getMonth())
                .days(adaptDays(responseEntity.getDays()))
                .build();
    }

    private List<Day> adaptDays(List<RyanAirScheduleDayResponseEntity> daysResponse) {
        return daysResponse
                .stream()
                .map(day -> scheduleDayResponse2DomainAdapter.execute(day))
                .collect(toList());
    }
}
