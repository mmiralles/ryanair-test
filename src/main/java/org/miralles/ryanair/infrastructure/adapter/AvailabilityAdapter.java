package org.miralles.ryanair.infrastructure.adapter;

import lombok.AllArgsConstructor;
import org.miralles.ryanair.domain.entity.Day;
import org.miralles.ryanair.domain.entity.Flight;
import org.miralles.ryanair.domain.entity.FlightAvailability;
import org.miralles.ryanair.domain.entity.Leg;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.of;

@AllArgsConstructor
public class AvailabilityAdapter {
    private static final int HOUR_BEGIN = 0;
    private static final int HOUR_END = 2;
    private static final int MINUTE_BEGIN = 3;
    private static final int MINUTE_END = 5;
    private static final int NO_STOPS = 0;

    public List<FlightAvailability> execute(final String departureIata,
                                             final String arrivalIata,
                                             final Integer month,
                                             final Integer year,
                                             final List<Day> daysInRange) {
        List<FlightAvailability> flightsAvailability = new ArrayList<>();
        daysInRange.forEach(day -> {
            LocalDate departureDate = LocalDate.of(year, month, day.getDay());
            FlightAvailability flightAvailability = FlightAvailability.builder()
                    .stops(NO_STOPS)
                    .legs(generateLegsBy(day.getFlights(), departureDate, departureIata, arrivalIata))
                    .build();
            flightsAvailability.add(flightAvailability);
        });
        return flightsAvailability;
    }

    private List<Leg> generateLegsBy(final List<Flight> flights,
                                     final LocalDate departureDate,
                                     final String departureIata,
                                     final String arrivalIata) {
        List<Leg> legs = new ArrayList<>();
        flights.forEach(flight -> {
            LocalTime departureTime = createLocalTimeFrom(flight.departureTime);
            LocalTime arrivalTime = createLocalTimeFrom(flight.arrivalTime);
            Leg leg = Leg.builder()
                    .arrivalAirport(arrivalIata)
                    .arrivalDateTime(of(departureDate, arrivalTime).toString())
                    .departureAirport(departureIata)
                    .departureDateTime(of(departureDate, departureTime).toString())
                    .build();
            legs.add(leg);
        });
        return legs;
    }

    private LocalTime createLocalTimeFrom(String time) {
        Integer hour = new Integer(time.substring(HOUR_BEGIN, HOUR_END));
        Integer minute = new Integer(time.substring(MINUTE_BEGIN, MINUTE_END));
        return LocalTime.of(hour, minute);
    }
}
