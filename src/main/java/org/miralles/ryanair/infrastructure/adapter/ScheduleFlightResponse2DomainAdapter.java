package org.miralles.ryanair.infrastructure.adapter;

import org.miralles.ryanair.domain.entity.Flight;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirScheduleFlightResponseEntity;

public class ScheduleFlightResponse2DomainAdapter {

    public Flight execute(RyanAirScheduleFlightResponseEntity flightResponseEntity) {
        return Flight.builder()
                .arrivalTime(flightResponseEntity.arrivalTime)
                .departureTime(flightResponseEntity.departureTime)
                .number(flightResponseEntity.number)
                .build();
    }
}
