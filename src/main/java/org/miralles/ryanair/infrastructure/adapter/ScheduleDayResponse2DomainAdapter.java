package org.miralles.ryanair.infrastructure.adapter;

import org.miralles.ryanair.domain.entity.Day;
import org.miralles.ryanair.domain.entity.Flight;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirScheduleDayResponseEntity;
import org.miralles.ryanair.infrastructure.repository.rest.RyanAirScheduleFlightResponseEntity;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ScheduleDayResponse2DomainAdapter {

    private ScheduleFlightResponse2DomainAdapter scheduleFlightResponse2DomainAdapter;

    public ScheduleDayResponse2DomainAdapter(ScheduleFlightResponse2DomainAdapter scheduleFlightResponse2DomainAdapter) {
        this.scheduleFlightResponse2DomainAdapter = scheduleFlightResponse2DomainAdapter;
    }

    public Day execute(final RyanAirScheduleDayResponseEntity entity){
        return Day.builder()
                .day(entity.getDay())
                .flights(adaptFlights(entity.getFlights()))
                .build();
    }

    private List<Flight> adaptFlights(List<RyanAirScheduleFlightResponseEntity> flightsResponse) {
        return flightsResponse
                .stream()
                .map(flight -> scheduleFlightResponse2DomainAdapter.execute(flight))
                .collect(toList());
    }
}
