**Code by Mauricio Miralles**

This is the code I had time to write, as you can see into the TODO tags, there is a lot of features to be improved. At the first days I started to
coding using TDD, but as far as I got into the code I left TDD, shame on me ;) .

As you can see I only filter at this moment by date and not by flight time. 

The application is writen with hexagonal architecture in order to implement the SOLID principles.

Also I left all the commits in the repository, then you can see the evolution of the development of this test.

I know there is a lot of unit test missing, neither the integration ones. For the tests I had been ussing Junit5 and Mockito.

---

## Ryanair Flight test urls:

To test the application I had been using those use cases.

1. http://localhost:8080/ryanair/interconnections?departure=WRO&arrival=DUB&departureDateTime=2018-03-01T07:00&arrivalDateTime=2018-03-03T21:00
2. http://localhost:8080/ryanair/interconnections?departure=WRO&arrival=DUB&departureDateTime=2018-03-01T07:00&arrivalDateTime=2018-30-03T21:00
3. http://localhost:8080/ryanair/interconnections?departure=WRO&arrival=DUB&departureDateTime=2018-03-01T07:00&arrivalDateTime=2018-03-01T21:00
4. http://localhost:8080/ryanair/interconnections?departure=WRO&arrival=DUB&departureDateTime=2018-03-03T07:00&arrivalDateTime=2018-03-03T21:00


---
